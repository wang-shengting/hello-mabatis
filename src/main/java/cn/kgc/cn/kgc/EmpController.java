package cn.kgc.cn.kgc;

import cn.kgc.entities.Emp;
import cn.kgc.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Controller  //定义控制器类 ，在spring 项目中由控制器负责将用户发来的URL请求转发到对应的服务接口
public class EmpController {
    @Resource
    private EmpService empService;

    @GetMapping("/emp")
    public String getAllEmp(Model model){
        List<Emp> list = empService.getAllEmp();
        model.addAttribute("empList",list);
        return "empList";
    }

    /**
     * @RequestParam 是从 request 里面拿取值，而 @PathVariable 是从一个URI模板里面来填充
     * @RequestParam 和 @PathVariable 注解是用于从 request 中接收请求的，两个都可以接收参数，
     */

    @GetMapping("/emp/{id}")
    public String getEmpId(@PathVariable("id") Integer id, Model model){
        Optional<Emp> emp = empService.getEmpById(id);
        model.addAttribute("emp",emp);
        return "emp";
    }

    @PostMapping("/emp")
    public String getEmp(Emp emp){
        empService.addEmp(emp);
        return "redirect:/emp";
    }

   @PutMapping("/emp")
    public String updateEmp(Emp emp){
        empService.updateEmp(emp);
        return "redirect:/emp";
    }

    @DeleteMapping("/emp/{id}")
    public String deleteEmpById(@PathVariable("id") Integer id){
        empService.deleteEmpById(id);
        return "redirect:/emp";
    }

    //跳转到新增
    @GetMapping("/emp/view")
    public String toEmpUI(Model model){
        model.addAttribute("emp",new Emp());
        return "emp";
    }



}
