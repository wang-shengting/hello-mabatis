package cn.kgc.mapper;

import cn.kgc.entities.Emp;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Optional;

@Mapper
public interface EmpMapper {
    /*增加*/
    public List<Emp> getAllEmp();
    public Optional<Emp> getEmpById(Integer id);
    public void addEmp(Emp emp);
    public void updateEmp(Emp emp);
    public void deleteEmpById(Integer id);
}
