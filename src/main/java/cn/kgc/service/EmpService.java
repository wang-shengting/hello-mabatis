package cn.kgc.service;

import cn.kgc.entities.Emp;

import java.util.List;
import java.util.Optional;

public interface EmpService {
    public List<Emp> getAllEmp();
    public Optional<Emp> getEmpById(Integer id);
    public void addEmp(Emp emp);
    public void updateEmp(Emp emp);
    public void deleteEmpById(Integer id);
}
