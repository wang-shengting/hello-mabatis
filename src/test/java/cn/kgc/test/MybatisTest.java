package cn.kgc.test;

import cn.kgc.entities.Emp;
import cn.kgc.mapper.EmpMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MybatisTest {
    @Resource
    private EmpMapper empMapper;


    @Test
    public void test() {
        List<Emp> list = empMapper.getAllEmp();
        list.forEach(System.out::println);
    }

    @Test
    public void listId() {
        Optional<Emp> emp = empMapper.getEmpById(7);
        System.out.println(emp);

    }
}

